// ____________________Import area___________________________
import { reloadMain } from "./scripts/itemCreators.js";
// ____________________________________________________________
const mainPageContainer = document.querySelector('.screen__one__container-content')
let url = 'https://jsonplaceholder.typicode.com/users'
axios.get(url)
    .then(res => {
        if (res.status === 200 || res.status === 201) {
            reloadMain(res.data, mainPageContainer)
            console.log(res.data);
        }
    })
    .catch(error => console.log(error))