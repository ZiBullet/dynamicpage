function reloadMain(users, place) {
    place.innerHTML = ''
    for (let user of users) {
        // Creating...                          ____________________User card (box)__________________
        let userCard = document.createElement('div')
        // __________________User info_______________________
        let userName = document.createElement('h4')
        let userInfoBox = document.createElement('div')
        let userCompany = document.createElement('span')
        let userWebsite = document.createElement('span')
        let userPhone = document.createElement('span')
        // _________________________Button_________________
        let btnMore = document.createElement('button')
        // Decorating...               _____________________User card______________________
        userCard.classList.add('user__card')
        userName.innerHTML = user.name
        userInfoBox.classList.add('user__card-info')
        userCompany.innerHTML = user.company.name
        userWebsite.innerHTML - user.website
        userPhone.innerHTML = user.phone
        btnMore.innerHTML = 'Подробнее'
        // Connecting...                 ______________User to document________________
        userCard.append(userName, userInfoBox, btnMore)
        userInfoBox.append(userCompany, userWebsite, userPhone)
        place.append(userCard)
    }
}
export {reloadMain}